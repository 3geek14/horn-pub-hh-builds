---
title: 'Horn Pub Meta HH Sets <br /> for MHR Sunbreak'
subtitle: (Preliminary)
lang: en-us
...

![Title Banner](SB_HH_Header.png)\

Compiled by the [Horn Pub community](https://discord.gg/adzmC95) on Discord,
with most of the credit going to glass and Gravyon. Title image from T3h Phish.
You can find the [pre-Sunbreak album on
imgur](https://imgur.com/gallery/fG2QXru).

In 3.0, Dragonheart reigned supreme, and healing horn was a meme. Oh how the
times have changed…

For finding armor and decorations to use, check out [the MHRise Wiki-Db set
builder](https://mhrise.wiki-db.com/sim/?hl=en). Add the skills called for and
tell the site what charms you have, and it'll pick out the armour pieces.

This is a preliminary document. So far, we're pretty confident that the horns
chosen here are optimal. However, we still intend to add a few more builds. All
of these builds were computed based on fixed charms, and we plan to give
guidance for better and worse charms in the future. Also, while we strongly
encourage you to try out Dereliction, we know that some people are strongly
opposed to playing without full HP, and we intend to include some
non-Dereliction builds to use instead.

## ![Switch Skills and Play Guide](Switch Skills and Guide.png)

(This section is still very much a work in progress. More will be added soon.)

Perhaps the biggest things brought to Hunting Horn in Sunbreak are Dereliction
and Silkbind Shockwave. With how busted Dereliction is, a lot of people are
going to run sets slowly draining their HP, making healing songs far more
attractive. (Especially when compared to the nonbo of healing people running
Dragonheart.) Silkbind Shockwave letting our shockwaves deal elemental damage
(and the other buffs to elemental builds) mean that we can finally embrace
elemental damage and get a bit more variety in our hunts.

Now that we can use scrolls to swap out our switch skills, Bead of Resonance has
room to play. The most important thing for Bead is that it provides Attack Up
song whenever it plays any song at all. The bead sticks around for one minute,
and the song lasts for one minute past the final time it activated, giving
potentially up to two minutes of Attack Up per use. On horns without Attack Up,
this is crucial in multiplayer, but it can be hard to keep an eye on in solo
play.

## ![Raw Builds](Raw Builds.png)

The basic set of skills for raw builds is:

* Attack Boost 7
* Resentment 5
* Weakness Exploit 3
* Dereliction 3
* Chain Crit 1
* Horn Maestro 1

To get this, you'll probably want everything but the waist from the Archfiend
set. Which waist you use will depend in part on your skill preferences and
available charms. If you're playing in multiplayer, also add Flinch Free 1. This
leaves you with a few decoration slots. For more damage, invest first into
Critical Boost, and then into Critical Eye. If you want more comfort skills,
Wirebug Whisperer is a good compromise between damage and comfy, and you can
also look at Evade Extender, Redirection, extra Handicraft, and Speed
Sharpening.

### Horn of the Indomitable +

This is the main horn to look at, and it's from the Anjanath tree. It's got the
ever-important Attack Up on green, and the newly-important Health Recovery (S)
on red. If you pick this horn, you'll want to throw in two levels of Handicraft
so that you aren't constantly dropping into white sharpness.

```{=html}
<details>
<summary><span>Sample Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Rathalos Coil X
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Weakness Exploit 3
* Dereliction 3
* (Windproof 3)
* Critical Boost 3
* Critical Eye 2
* Handicraft 2
* Chain Crit 1
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

### Other Healing Horns

Fine Kamura Flute is pretty strong, and it doesn't need Handicraft. It has less
raw, but heal on green is potentially stronger with Performance Mode.

```{=html}
<details>
<summary><span>Sample Build</span></summary>
```
:::{.detail}
Armor:

* Kaiser Crown X
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Rathalos Coil X
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Critical Eye 5
* Resentment 5
* Critical Boost 3
* Weakness Exploit 3
* Dereliction 3
* (Windproof 3)
* Chain Crit 1
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

Teostra's Musica and Baselsium Rookslayer are weaker, but sometimes you have a
need for some fashion.

### Non-Healing Horns:

Kampa Da Lavater (Pyre Rakna-Kadaki tree) is similar to the Anjanath horn. It's
got more sharpness, but it'll do less damage on Sonic Blooms and Earthshakers.
The Final Boss Horn has very comfy sharpness and similar damage to Anjanath, but
it has worse slots. Tigrex Roar + has fewer slots and no elemental damage when
compared to Anjanath, but it has Sonic Waves. Skytremor Typhoon (Ibushi tree)
can be good if you use Bead of Resonance to get an Attack Up, but then you don't
have as many bugs to spend on Sonic Bloom or Earthshaker.

### Non-Dereliction Builds:

Some people don't want their HP to constantly be draining, and that's
understandable. We encourage you to try out a Dereliction build on a horn with
healing, but if you really don't want that, here's a template build. This
template does not assume any slots on the weapon, and it only uses a 2-2-0
charm, so you can likely fit a few more skills onto it.

```{=html}
<details>
<summary><span>Build Template</span></summary>
```
:::{.detail}
Armor:

* Lunagaron Helm
* Archfiend Armor Baulo
* Barioth Vambraces X
* Rathalos Coil X
* Ingot Greaves X

Skills:

* Critical Eye 7
* Attack Boost 7
* Weakness Exploit 3
* Critical Boost 3
* Wirebug Whisperer 1
* Chain Crit 1
* Maximum Might 1
* Horn Maestro 1

[Armor Set Searcher for this build](https://mhrise.wiki-db.com/sim/showpb/Cg5MdW5hZ2Fyb24gSGVsbRIVQXJjaGZpZW5kIEFybW9yIEJhdWxvGhNCYXJpb3RoIFZhbWJyYWNlcyBYIg9SYXRoYWxvcyBDb2lsIFgqD0luZ290IEdyZWF2ZXMgWDoGGgQIAhACQhBOb25lIFNsb3QgV2VhcG9uShIKDkF0dGFjayBKZXdlbCAyEANKFAoQQ3JpdGljYWwgSmV3ZWwgMhACShIKDkV4cGVydCBKZXdlbCAyEAFKFAoQU29ub3JvdXMgSmV3ZWwgMRABShYKElRlbmRlcml6ZXIgSmV3ZWwgMhAC?hl=en)
:::
```{=html}
</details>
```

## ![Elemental Builds](Elemental Builds.png)

For almost all matchups, elemental builds will surpass raw builds substantially.
You'll need to keep Silkbind Shockwave active pretty much the whole time, or
else you'll be losing out on a ton of your elemental damage. Slide Beat is still
good, but you may have trouble with all the switching. Whenever possible, use
the appropriate anti-species ramp-up skills.

Here are the skills to start out with:

* Elemental Attack 5
* Resentment 5
* Attack Boost 4
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Horn Maestro 1
* Flinch Free 1 (if multiplayer)

Generally on Hunting Horn, we recommend having at least 30 hits of your best
sharpness, or else sharpening will be a pain. If you have lots of extra slots
and fewer than 30 hits, you could add Protective Polish 3, assuming that
Handicraft can't get you to 30. However, unless you have an absurdly good charm,
you'll pretty much always be better off giving yourself a nice big chunk of
white sharpness and spending the points on other skills. If you wind up using a
horn on white sharpness, get Attack Boost 7 before adding any comfort skills.

Comfort skills are going to be the same as they were for raw builds. If you have
spare points, go for Attack Boost, then Critical Boost, then Critical Eye.

If you're trying to use the stronger horns without healing and are having
trouble managing Dereliction's health drain on those elements, try using gourmet
fish, healing pets, or eating for Dango Shifter.

### ![Fire Builds](Fire.png)

Horn of the Indomitable + (Anjanath tree) has healing, but Doomed Bell
(Rakna-Kadaki tree) does more damage. If you never use bead, the damage output
is similar. With Doomed Bell, prioritize Critical Boost 3 over Attack Boost 7,
with any extra slots.

```{=html}
<details>
<summary><span>Sample Horn of the Indomitable + Build</span></summary>
```
:::{.detail}
Armor:

* Sinister Demon Helm
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Fire Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Handicraft 2
* Flinch Free 2
* Critical Boost 1
* Horn Maestro 1
:::
```{=html}
</details>
```

```{=html}
<details>
<summary><span>Sample Doomed Bell Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Fire Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Critical Boost 3
* Flinch Free 1
* Critical Eye 1
* Horn Maestro 1
:::
```{=html}
</details>
```

### ![Water Builds](Water.png)

Sublime Bell + (Mizutsune tree) has Health Recovery (L), and Echoing Droth Roar
+ (Royal Ludroth) has slightly more damage. Amphibia Larghetto + (Tetranadon
tree) is the only water horn with Attack Up song, but it's worse than the other
two even if you don't use Bead of Resonance with the others, and it'll take a
lot of sharpness investment.

```{=html}
<details>
<summary><span>Sample Sublime Bell + Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Water Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Flinch Free 1
* Critical Boost 1
* Handicraft 1
* Horn Maestro 1
:::
```{=html}
</details>
```

```{=html}
<details>
<summary><span>Sample Echoing Droth Roar + Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Water Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Critical Boost 3
* Flinch Free 1
* Critical Eye 1
* Horn Maestro 1
:::
```{=html}
</details>
```

### ![Thunder Builds](Thunder.png)

Skytremor Rumble (Narwa tree) is strong and has big heals, but you need to keep
Bead of Resonance active. Demonlord Djembe (Furious Rajang tree) has no healing,
but it's stronger if you refuse to use Bead of Resonance. With Demonlord Djembe,
prioritize Critical Boost 3 over Attack Boost 7, with any extra slots.

```{=html}
<details>
<summary><span>Sample Skytremor Rumble Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Resentment 5
* Attack Boost 5
* Thunder Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Handicraft 3
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

```{=html}
<details>
<summary><span>Sample Demonlord Djembe Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Thunder Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Critical Boost 3
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

### ![Ice Builds](Ice.png)

Flicker Blizzard Flute (Aurora Somnacanth tree) is the strongest and comes with
Health Regeneration. Because of how much investment it takes to get purple
sharpness, you'll get better damage by focusing on other skills and having a
chunky bar of white, unless you have incredible charms.

```{=html}
<details>
<summary><span>Sample Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Ice Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Critical Boost 3
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

Frigigurio (Barioth tree), Harmonic Resonance (Lunagaron tree), and Daora's
Diabassoon (Kushala Daora tree) are each about 5% behind Flicker Blizzard Flute,
but maybe you have a preference for one of their song lists. In multiplayer,
Frigigurio and Harmonic Resonance's buffs might overcome Flicker Blizzard Flute.
Against monsters with very low elemental hitzones, like Astalos, Flicker
Blizzard Flute isn't as good, and the other three are more viable.

### ![Dragon Builds](Dragon.png)

Vicello Kvar Black (Bnahabra tree) has Health Recovery (L), and Le Grégorien
(Shagaru Magala tree) has more damage. If you go Vicello Kvar Black, you'll get
more damage by investing into purple sharpness than sticking with white, but
white is comfier.

```{=html}
<details>
<summary><span>Sample Vicello Kvar Black Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Dragon Attack 5
* Handicraft 4
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

```{=html}
<details>
<summary><span>Sample Le Grégorien Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Resentment 5
* Dragon Attack 5
* Attack Boost 4
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Critical Boost 3
* Flinch Free 1
* Handicraft 1
* Horn Maestro 1
:::
```{=html}
</details>
```

Magician's Allure (Malzeno tree) is good for multiplayer with its sharpness
buffs, but the damage is comparable to Vicello Kvar Black.

Crimson Clearwing (Crimson Glow Valstrax tree) does similar to damage to Vicello
Kvar Black without the hassle of keeping a bead up. If you don't manage to keep
bead up with Le Grégorien, Crimson Clearwing will have comparable damage to that
too, and the sharpness can give you higher uptime.

```{=html}
<details>
<summary><span>Sample Build</span></summary>
```
:::{.detail}
Armor:

* Archfiend Armor Cerato
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 6
* Resentment 5
* Dragon Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Critical Boost 3
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

The Final Boss horn is easy to build for, but the damage is worse than Crimson
Clearwing, outside of Sonic Bloom and Earthshaker. With Crimson Clearwing and Le
Grégorien, prioritize Critical Boost 3 over Attack Boost 7, with any extra
slots.

## ![Tryhard Builds](Tryhard Builds.png)

If you love big numbers and hate being comfortable, have we got the builds for
you!

Add Coalescence 2 and Bloodlust 1 to the set searcher, and it'll use the Storge
or Arc Helm. The health drain stacks, so be prepared to eat gourmet fish.
Unfortunately, this won't work for all horns, as it'll depend on how much
Handicraft you need to fit in and how good your charm is. For some horns, this
is only for people hacking charms in. For Dragon and Fire, consider the higher
damage horns, even though they don't heal. For one thing, this does not work as
a comfy set. For another thing, they do more damage even without Bloodlust.

```{=html}
<details>
<summary><span>Sample Doomed Bell (Fire) Build</span></summary>
```
:::{.detail}
Armor:

* Storge Helm / Arc Helm
* Archfiend Armor Baulo
* Archfiend Armor Epine
* Archfiend Armor Ura
* Archfiend Armor Sceros

Skills:

* Attack Boost 7
* Resentment 5
* Fire Attack 5
* Weakness Exploit 3
* Chain Crit 3
* Dereliction 3
* Coalescence 2
* Bloodlust 1
* Flinch Free 1
* Horn Maestro 1
:::
```{=html}
</details>
```

Note: we don't recommend this. While it's certainly stronger, it has a much
smaller margin for error than the other builds. If you use this build and find
yourself carting a lot, please don't blame us.

## ![Closing Thoughts](Closing Thoughts.png)

To start out, probably go for one of the raw builds, just so you have something
you can bring to any hunt. As you progress, start filling out your various
elemental builds. There are a lot of good options now, even once you've chosen
an element.

I'd again like to reiterate that this album is a work in progress. The [Horn Pub
community](https://discord.gg/adzmC95) has been hard at work, and we're still
going. This album is going to get more fleshed out, hopefully pretty soon!
Special thanks again to glass and Gravyon for doing most of the calculations for
this.